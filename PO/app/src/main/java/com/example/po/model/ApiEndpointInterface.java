package com.example.po.model;

import com.example.po.model.DataModels.MapaObszaru;
import com.example.po.model.DataModels.Odcinek;
import com.example.po.model.DataModels.OdcinekTrasy;
import com.example.po.model.DataModels.Odznaka;
import com.example.po.model.DataModels.Trasa;
import com.example.po.model.DataModels.WeryfikowanaTrasa;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiEndpointInterface {
    @GET("/api/odznaki")
    Call<List<Odznaka>> GetAchievementsList();

    @GET("/api/WeryfikowaneTrasy")
    Call<List<WeryfikowanaTrasa>> GetRoutesToVerifyList();

    @DELETE("api/WeryfikowaneTrasy/{id}/{idT}")
    Call<ResponseBody> DeleteRoute(@Path("id") String id, @Path("idT") String idT);

    @GET("/api/Trasy/{id}")
    Call<Trasa> GetRoute(@Path("id") String id);

    @GET("/api/mapyobszarow")
    Call<List<MapaObszaru>> GetAreaMapList();

    @GET("/api/mapyobszarow/{id}")
    Call<MapaObszaru> GetAreaMap(@Path("id") String id);

    @GET("/api/mapyobszarow/Trasy/{id}")
    Call<List<Trasa>> GetMapRoutesList(@Path("id") String id);

    @POST("api/trasy")
    Call<Trasa> PostRoute(@Body Trasa trasa);

    @POST("api/OdcinkiTras")
    Call<ResponseBody> PostRouteSegment(@Body OdcinekTrasy trasa);


}
