package com.example.po.model.DataModels;

import java.util.List;

public class MapaObszaru {
    public int id;
    public String opis;
    public String nazwaOb;
    public List<Odcinek> odcinkiTrasy;
}
