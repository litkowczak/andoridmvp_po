package com.example.po.model.DataModels;

import java.util.List;

public class Odcinek {
    public int id;
    public int mapaObszaruId;
    public MapaObszaru mapaObszaru;
    public String nazwaOd;
    public float dlugoscO;
    public List<OdcinekTrasy> odcinkiTras;
}
