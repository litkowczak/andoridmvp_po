package com.example.po.model.DataModels;

public class OdcinekTrasy {
    public int trasaId;
    public Trasa trasa;
    public int odcinekId;
    public Odcinek odcinek;
    public int kolejnosc;
}
