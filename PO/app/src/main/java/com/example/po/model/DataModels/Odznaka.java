package com.example.po.model.DataModels;

import java.sql.Date;


public class Odznaka {

    public int Id;

    public int IdTO;
    public TypOdznaki typOdznaki;

    public int IdU;
    public Uzytkownik uzytkownik;

    public Date dataZdob;

    public String opisOdz;
}