package com.example.po.model.DataModels;

import java.sql.Date;
import java.util.List;

public class Trasa {
    public int id;
    public int IdU;
    public Uzytkownik uzytkownik;
    public String nazwaTrasy;
    public float sumaPkt;
    public Date dataStart;
    public boolean ukonczonaT;
    public boolean odrzuconaT;
    public boolean zweryfikowanaT;
    public List<OdcinekTrasy> odcinkiTras;
}
