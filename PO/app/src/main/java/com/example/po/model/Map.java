package com.example.po.model;

public class Map {
    private String name;
    private int sectionsAmount;

    public Map(String name, int sectionsAmount) {
        this.name = name;
        this.sectionsAmount = sectionsAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSectionsAmount() {
        return sectionsAmount;
    }

    public void setSectionsAmount(int sectionsAmount) {
        this.sectionsAmount = sectionsAmount;
    }
}
