package com.example.po.model;

import java.util.ArrayList;

public class MapRepository {
    private ArrayList<Map> maps;
    public static final MapRepository INSTANCE = new MapRepository();

    private MapRepository() {
    }

    public ArrayList<Map> getMaps() {
        //TODO query maps(on background thread)
        if (maps == null) {
            maps = initMaps();
        }
        return maps;
    }

    private ArrayList<Map> initMaps() {
        ArrayList<Map> dmaps = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            dmaps.add(new Map("Mapka " + i,3+i%3));
        }
        return  dmaps;
    }
}
