package com.example.po.model;

public class Route {
    private String name;
    private int routeAmountSections;

    public Route(String name, int routeAmountSections) {
        this.name = name;
        this.routeAmountSections = routeAmountSections;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRouteAmountSections() {
        return routeAmountSections;
    }

    public void setRouteAmountSections(int routeAmountSections) {
        this.routeAmountSections = routeAmountSections;
    }
}
