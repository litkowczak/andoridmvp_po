package com.example.po.model;

import java.util.ArrayList;

public class RouteRepository {
    private ArrayList<Route> routes;

    public ArrayList<Route> getRoutes(){
        if(routes==null){
            routes = initRoutes();
        }
        return routes;
    }

    private ArrayList<Route> initRoutes() {
        ArrayList<Route> tmp = new ArrayList<>();
        tmp.add(new Route("Trasa 1", 3));
        tmp.add(new Route("Trasa 2", 2));
        tmp.add(new Route("Trasa 3", 4));
        tmp.add(new Route("Trasa 4", 1));
        tmp.add(new Route("Trasa 5", 5));
        tmp.add(new Route("Trasa 6", 1));
        return tmp;
    }
}
