package com.example.po.model;

public class Section {
    private String name;
    private int sectionLength;

    public Section(String name, int sectionLength) {
        this.name = name;
        this.sectionLength = sectionLength;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSectionLength() {
        return sectionLength;
    }

    public void setSectionLength(int sectionLength) {
        this.sectionLength = sectionLength;
    }
}
