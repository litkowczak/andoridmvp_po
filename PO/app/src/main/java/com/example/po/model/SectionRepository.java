package com.example.po.model;

import java.util.ArrayList;

public class SectionRepository
{
    private ArrayList<Section> sections;

    public ArrayList<Section> getSections(){
        if(sections==null){
            sections = initSections();
        }
        return sections;
    }

    private ArrayList<Section> initSections() {
        ArrayList<Section> tmp = new ArrayList<>();
        tmp.add(new Section("Odcinek 1", 1));
        tmp.add(new Section("Odcinek 2", 2));
        tmp.add(new Section("Odcinek 3", 4));
        tmp.add(new Section("Odcinek 4", 3));
        tmp.add(new Section("Odcinek 5", 6));
        tmp.add(new Section("Odcinek 6", 2));

        return tmp;
    }
}
