package com.example.po.presenter;


import android.util.Log;
import android.widget.Toast;

import com.example.po.model.APIClient;
import com.example.po.model.ApiEndpointInterface;
import com.example.po.model.DataModels.Odznaka;
import com.example.po.view.achivments.AchivmentsAdapter;
import com.example.po.view.achivments.IAchivmentsView;
import com.example.po.view.achivments.IAchivmentsViewItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AchivmentsActivityPresenter {

    ApiEndpointInterface apiEndpointInterface;

    private IAchivmentsView view;
    private String[] names = {"Kamil Lewandowski","Kamil Olszanski", "Jan Kowalski", "Ziemowit Syta", "Krzysztof Korbik"};
    private String[] dates = {"03.01.2020","09.12.2019","03.11.2019","03.12.2019","07.02.2020"};
    private String[] achivmentsNames = {"Odznaka 1", "Zdobycie szczytu", "Odznaka 2", "Odznaka 10","Odznaka 1"};

    public AchivmentsActivityPresenter(IAchivmentsView _view) {

        this.view = _view;
        apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
        Call<List<Odznaka>> call = apiEndpointInterface.GetAchievementsList();
        call.enqueue(new Callback<List<Odznaka>>(){
            @Override
            public void onResponse(Call<List<Odznaka>> call, Response<List<Odznaka>> response) {
                List <Odznaka> achievements = response.body();
                names = new String[achievements.size()];
                dates = new String[achievements.size()];
                achivmentsNames = new String[achievements.size()];
                for (int i = 0; i< achievements.size();i++) {
                    names[i] = achievements.get(i).uzytkownik.imieT + " " + achievements.get(i).uzytkownik.nazwiskoT;
                    dates[i] = achievements.get(i).dataZdob.toString();
                    achivmentsNames[i] = achievements.get(i).typOdznaki.nazwaO;
                }
                view.notifyChanged();
            }
            @Override
            public void onFailure(Call<List<Odznaka>> call, Throwable t) { call.cancel(); }
        });
    }

    public int getAchivmentCount() {
        return names.length;
    }

    public void onBindViewHolder(IAchivmentsViewItem viewHolder, int i) {
        viewHolder.setAchivmentDateTV(getAchivmentDate(i));
        viewHolder.setAchivmentName(getAchivmentName(i));
        viewHolder.setAchivmentUserNameTV(getAchivmentUserName(i));
    }

    private String getAchivmentUserName(int i) {
        return names[i];
    }

    private String getAchivmentName(int i) {
        return achivmentsNames[i];
    }

    private String getAchivmentDate(int i) {
        return dates[i];
    }

    public void openMyAchivmentsActivity() {
        view.startMyAchivments();
    }
}
