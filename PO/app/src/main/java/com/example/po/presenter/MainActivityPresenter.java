package com.example.po.presenter;



import android.app.Activity;
import android.content.Intent;

import com.example.po.view.achivments.AchivmentsActivity;
import com.example.po.view.main.IMainView;
import com.example.po.view.main.MainActivity;
import com.example.po.view.maps.MapsActivity;
import com.example.po.view.routesverification.RoutesVerificationActivity;


public class MainActivityPresenter {
    private IMainView view;

    public MainActivityPresenter(IMainView view){
        this.view = view;
    }

    public void openMapsActivity(Activity mainActivity) {
        Intent intent = new Intent(mainActivity, MapsActivity.class);
        mainActivity.startActivity(intent);

    }

    public void openRoutesVerificationActivity(MainActivity mainActivity) {
        Intent intent = new Intent(mainActivity, RoutesVerificationActivity.class);
        mainActivity.startActivity(intent);
    }

    public void openAchivmentsActivity(MainActivity mainActivity){
        Intent intent = new Intent(mainActivity, AchivmentsActivity.class);
        mainActivity.startActivity(intent);
    }
}
