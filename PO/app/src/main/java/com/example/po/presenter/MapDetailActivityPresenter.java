package com.example.po.presenter;

import android.view.View;
import android.widget.TextView;

import com.example.po.model.APIClient;
import com.example.po.model.ApiEndpointInterface;
import com.example.po.model.DataModels.Odznaka;
import com.example.po.model.DataModels.Trasa;
import com.example.po.model.MapRepository;
import com.example.po.model.Route;
import com.example.po.model.RouteRepository;
import com.example.po.view.mapdetail.IMapDetailView;
import com.example.po.view.mapdetail.IRouteItemView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapDetailActivityPresenter {

    private IMapDetailView view;
    private MapRepository mapRepository;
    private RouteRepository routeRepository;
    List<Trasa> routes;
    String index;

    ApiEndpointInterface apiEndpointInterface;

    public MapDetailActivityPresenter(IMapDetailView _view, String _index) {
        this.view = _view;
        routes = new ArrayList<Trasa>();
        routeRepository = new RouteRepository();
        index = _index;

        apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
        Call<List<Trasa>> call = apiEndpointInterface.GetMapRoutesList(_index);
        call.enqueue(new Callback<List<Trasa>>(){
            @Override
            public void onResponse(Call<List<Trasa>> call, Response<List<Trasa>> response) {
                routes = response.body();
                view.notifyChanged();
            }
            @Override
            public void onFailure(Call<List<Trasa>> call, Throwable t) { call.cancel(); }
        });
    }

    public int getRouteCount() {
        return routes.size();
    }

    public void onBindViewHolder(IRouteItemView myViewHolder, int i) {
        myViewHolder.setRouteAmountSection(getRouteSectionAmount(i));
        if(routes.size() > 0)
            myViewHolder.setRouteName(getRouteName(i));


    }

    public String getRouteName(int i){
        return routes.get(i).nazwaTrasy;
    }

    public int getRouteSectionAmount(int i){
        if(routes.size() > 0)
            return routes.get(i).odcinkiTras.size();
        return 0;
    }

    public void openCreateRouteActivity() {
        view.startRouteCreatorActivity(index);
    }
}
