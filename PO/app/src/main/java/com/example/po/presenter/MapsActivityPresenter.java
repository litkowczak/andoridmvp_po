package com.example.po.presenter;

import android.util.Log;
import android.view.View;

import com.example.po.model.APIClient;
import com.example.po.model.ApiEndpointInterface;
import com.example.po.model.DataModels.MapaObszaru;
import com.example.po.model.MapRepository;
import com.example.po.view.maps.IItemView;
import com.example.po.view.maps.IMapsView;
import com.example.po.view.maps.MapsRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivityPresenter {
    private IMapsView view;
    private MapRepository mapRepository;
    ApiEndpointInterface apiEndpointInterface;

    List<MapaObszaru> areaMaps;

    public MapsActivityPresenter(IMapsView _view) {
        areaMaps = new ArrayList<MapaObszaru>();
        this.view = _view;
        apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
        Call<List<MapaObszaru>> deleteRequest = apiEndpointInterface.GetAreaMapList();
        deleteRequest.enqueue(new Callback<List<MapaObszaru>>() {
            @Override
            public void onResponse(Call<List<MapaObszaru>> call, Response<List<MapaObszaru>> response) {
                areaMaps = response.body();
                view.notifyChanged();
            }

            @Override
            public void onFailure(Call<List<MapaObszaru>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public int getMapsCount() {
        return areaMaps.size();
    }

    public String getMap(int i) {
        return areaMaps.get(i).nazwaOb;
    }

    public int getMapSectionAmount(int i)
    {
        return areaMaps.get(i).odcinkiTrasy.size();
    }

    public void onBindViewHolder(IItemView itemView, final int index) {
        itemView.setMapName(getMap(index));
        itemView.setMapSetionAmount(getMapSectionAmount(index));

        itemView.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.startMapDetailActivity(areaMaps.get(index).nazwaOb, Integer.toString(areaMaps.get(index).id));
            }
        });
    }
}
