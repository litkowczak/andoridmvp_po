package com.example.po.presenter;

import com.example.po.model.APIClient;
import com.example.po.model.ApiEndpointInterface;
import com.example.po.model.DataModels.Odznaka;
import com.example.po.view.myachivments.IMyAchivmentsView;
import com.example.po.view.myachivments.MyAchivmentsItemView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAchivmentsActivityPresenter {

    ApiEndpointInterface apiEndpointInterface;

    private IMyAchivmentsView view;
    private String name = "Jan Kowalski";
    private String[] myAchivments = {"Osiagnięcie1","Osiagnięcie 2","Osiagnięcie 3",
            "Osiagnięcie 4","Osiagnięcie 5","Osiagnięcie 6"};
    private String[] myAchivmentsDate = {"12.12.2019","10.10.2019","06.01.2020",
            "09.11.2019","29.10.2019","06.12.2019"};
    public MyAchivmentsActivityPresenter(IMyAchivmentsView _view) {

        this.view = _view;
        apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
        Call<List<Odznaka>> call = apiEndpointInterface.GetAchievementsList();
        call.enqueue(new Callback<List<Odznaka>>(){
            @Override
            public void onResponse(Call<List<Odznaka>> call, Response<List<Odznaka>> response) {
                List <Odznaka> achievements = response.body();
                myAchivmentsDate = new String[achievements.size()];
                myAchivments = new String[achievements.size()];
                for (int i = 0; i< achievements.size();i++) {
                    myAchivmentsDate[i] = achievements.get(i).dataZdob.toString();
                    myAchivments[i] = achievements.get(i).typOdznaki.nazwaO;
                }
                view.notifyChanged();
            }
            @Override
            public void onFailure(Call<List<Odznaka>> call, Throwable t) { call.cancel(); }
        });
    }

    public void setActualUsername() {
        view.setUsername(name);
    }

    public void onBindViewHolder(MyAchivmentsItemView viewHolder, int i) {
        viewHolder.setMyAchivmentDate(getAchivmentDate(i));
        viewHolder.setMyAchivmentName(getAchivmentName(i));
    }

    private String getAchivmentName(int i) {
        return myAchivments[i];
    }

    private String getAchivmentDate(int i) {
        return myAchivmentsDate[i];
    }

    public int getMyAchivmentsCount() {
        return myAchivments.length;
    }

    public void onShareBtnClick() {

    }
}
