package com.example.po.presenter;

import com.example.po.view.rating.IRatingView;

public class RatingActivityPresenter {
    private IRatingView view;
    private double diff = 2.5;
    private double state = 4.5;
    private double attractivity = 5;

    public RatingActivityPresenter(IRatingView view) {
        this.view = view;
    }

    public void setUpRatingBars() {
        view.setRatingBars(diff,state,attractivity);
    }
}
