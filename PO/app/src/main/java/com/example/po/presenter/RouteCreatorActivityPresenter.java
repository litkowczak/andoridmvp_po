package com.example.po.presenter;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.po.model.APIClient;
import com.example.po.model.ApiEndpointInterface;
import com.example.po.model.DataModels.MapaObszaru;
import com.example.po.model.DataModels.OdcinekTrasy;
import com.example.po.model.DataModels.Trasa;
import com.example.po.model.Map;
import com.example.po.model.SectionRepository;
import com.example.po.view.routecreator.IRouteCreator;
import com.example.po.view.routecreator.SectionRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteCreatorActivityPresenter {
    IRouteCreator view;
    ApiEndpointInterface apiEndpointInterface;
    MapaObszaru areaMap;
    boolean[] checkedBoxes;
    String index;

    public RouteCreatorActivityPresenter(IRouteCreator _view, String _index) {
        this.view = _view;
        this.index = _index;
        areaMap = new MapaObszaru();
        areaMap.odcinkiTrasy = new ArrayList<>();
        apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
        Call<MapaObszaru> call = apiEndpointInterface.GetAreaMap(index);
        call.enqueue(new Callback<MapaObszaru>(){
            @Override
            public void onResponse(Call<MapaObszaru> call, Response<MapaObszaru> response) {
                areaMap = response.body();
                checkedBoxes = new boolean[areaMap.odcinkiTrasy.size()];
                for (boolean b:checkedBoxes) {
                    b = false;
                }
                view.notifyChanged();
            }
            @Override
            public void onFailure(Call<MapaObszaru> call, Throwable t) { call.cancel(); }
        });
    }


    public void onBindViewHolder(final SectionRecyclerViewAdapter.ViewHolder viewHolder, int i) {
        viewHolder.setSectionName(getSetionName(i));
        viewHolder.setSetionLenght(getSetionLenght(i));

        viewHolder.checkBox.setTag(i);
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Integer pos = (Integer) viewHolder.checkBox.getTag();
                checkedBoxes[pos] = true;
            }
        });

    }

    private int getSetionLenght(int index) {
        return (int)areaMap.odcinkiTrasy.get(index).dlugoscO;
    }

    private String getSetionName(int index) {
        return areaMap.odcinkiTrasy.get(index).nazwaOd;
    }

    public int getSectionCount() {
        return areaMap.odcinkiTrasy.size();
    }

    public void saveRoute(String name)
    {
        boolean isRouteChecked = false;

        for (boolean b:checkedBoxes) {
            if(b)
                isRouteChecked = true;
        }

        if(isRouteChecked)
        {
            final Trasa trasa = new Trasa();
            //trasa.Id = null;
            trasa.IdU = 1;
            trasa.nazwaTrasy = name;
            trasa.sumaPkt = 54;
            apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
            Call<Trasa> call = apiEndpointInterface.PostRoute(trasa);
            call.enqueue(new Callback<Trasa>(){
                @Override
                public void onResponse(Call<Trasa> call, Response<Trasa> response) {
                    Trasa t = response.body();

                    for (int i = 0; i<checkedBoxes.length; i++) {
                        if(checkedBoxes[i])
                        {
                            OdcinekTrasy routeSegment = new OdcinekTrasy();
                            routeSegment.odcinekId = areaMap.odcinkiTrasy.get(i).id;
                            routeSegment.trasaId = t.id;

                            Call<ResponseBody> call2 = apiEndpointInterface.PostRouteSegment(routeSegment);
                            call2.enqueue(new Callback<ResponseBody>(){
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) { call.cancel(); }
                            });
                        }
                    }
                /*OdcinekTrasy routeSegment = new OdcinekTrasy();
                routeSegment.odcinekId = Integer.parseInt(index);//1
                routeSegment.trasaId = t.id;//0
                Call<ResponseBody> call2 = apiEndpointInterface.PostRouteSegment(routeSegment);
                call2.enqueue(new Callback<ResponseBody>(){
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) { call.cancel(); }
                });*/
                }
                @Override
                public void onFailure(Call<Trasa> call, Throwable t) { call.cancel(); }
            });
        }
    }


}
