package com.example.po.presenter;

import com.example.po.model.APIClient;
import com.example.po.model.ApiEndpointInterface;
import com.example.po.model.DataModels.Odznaka;
import com.example.po.model.DataModels.Trasa;
import com.example.po.view.routetoverifydetails.IRouteToVerifyDetailsItemView;
import com.example.po.view.routetoverifydetails.IRoutesToVerifyDetails;

import java.sql.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteToVerifyDetailsActivityPresenter {

    private IRoutesToVerifyDetails view;
    private String[] sections = {};
    private String date;
    private String touristName;
    ApiEndpointInterface apiEndpointInterface;
    String userId;
    String routeId;

    public RouteToVerifyDetailsActivityPresenter(IRoutesToVerifyDetails _view, int _userId, int _routeId) {
        this.view = _view;
        userId = Integer.toString(_userId);
        routeId = Integer.toString(_routeId);
        date = "2016-01-01";
        apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
        Call<Trasa> call = apiEndpointInterface.GetRoute(Integer.toString(_routeId));
        call.enqueue(new Callback<Trasa>(){
            @Override
            public void onResponse(Call<Trasa> call, Response<Trasa> response) {
                Trasa route = response.body();
                sections = new String[route.odcinkiTras.size()];
                date = route.dataStart.toString();
                touristName = route.uzytkownik.imieT + " " + route.uzytkownik.nazwiskoT;
                for (int i = 0; i< route.odcinkiTras.size();i++) {
                    sections[i] = route.odcinkiTras.get(i).odcinek.nazwaOd;
                }
                view.notifyChanged();
                setUpTextViews();
            }
            @Override
            public void onFailure(Call<Trasa> call, Throwable t) { call.cancel(); }
        });



    }


    public void setUpTextViews() {
        view.setTouristName(touristName);
        view.setReportDate(date.toString());

    }

    public int getSectionCount() {
        return sections.length;
    }

    public void onBindViewHolder(IRouteToVerifyDetailsItemView viewHolder, int i) {
        viewHolder.setSectionNameTV(getSectionName(i));
    }

    private String getSectionName(int i) {
        return sections[i];
    }


    public void startRatingActivity() {
        view.startRatingActivity();
    }

    public void acceptRoute() {
        Call<ResponseBody> deleteRequest = apiEndpointInterface.DeleteRoute(userId,routeId);
        deleteRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // use response.code, response.headers, etc.
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                // handle failure
            }
        });
    }

    public void rejectRoute() {
        Call<ResponseBody> deleteRequest = apiEndpointInterface.DeleteRoute(userId,routeId);
        deleteRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // use response.code, response.headers, etc.
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                // handle failure
            }
        });
    }
}
