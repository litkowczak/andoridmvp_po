package com.example.po.presenter;

import android.view.View;

import com.example.po.model.APIClient;
import com.example.po.model.ApiEndpointInterface;
import com.example.po.model.DataModels.Odznaka;
import com.example.po.model.DataModels.Trasa;
import com.example.po.model.DataModels.WeryfikowanaTrasa;
import com.example.po.view.routesverification.IRoutesVerificationItemView;
import com.example.po.view.routesverification.IRoutesVerificationView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RoutesVerificationActivityPresenter {
    private IRoutesVerificationView view;
    ApiEndpointInterface apiEndpointInterface;
    //przykladowe dane
    private String [] tourist = {};
    private String [] dates = {};
    private String [] routeNames = {};
    List <WeryfikowanaTrasa> routes;
    Integer userId;
    Integer routeId;

    public RoutesVerificationActivityPresenter(IRoutesVerificationView _view) {
        this.view = _view;
        apiEndpointInterface = APIClient.getClient().create(ApiEndpointInterface.class);
        Call<List<WeryfikowanaTrasa>> call = apiEndpointInterface.GetRoutesToVerifyList();
        call.enqueue(new Callback<List<WeryfikowanaTrasa>>(){
            @Override
            public void onResponse(Call<List<WeryfikowanaTrasa>> call, Response<List<WeryfikowanaTrasa>> response) {
                routes = response.body();
                tourist = new String[routes.size()];
                dates = new String[routes.size()];
                routeNames = new String[routes.size()];

                for (int i = 0; i< routes.size();i++) {
                    tourist[i] = routes.get(i).trasa.uzytkownik.imieT + " " + routes.get(i).trasa.uzytkownik.nazwiskoT;
                    dates[i] = routes.get(i).trasa.dataStart.toString();
                    routeNames[i] = routes.get(i).trasa.nazwaTrasy;
                }
                view.notifyChanged();
            }
            @Override
            public void onFailure(Call<List<WeryfikowanaTrasa>> call, Throwable t) { call.cancel(); }
        });
    }

    public void onBindViewHolder(IRoutesVerificationItemView viewHolder, final int i) {
        viewHolder.setRouteNameTV(getRouteName(i));
        viewHolder.setRouteVerifyReportDate(getRouteReportDate(i));
        viewHolder.setTouristNameTV(getTouristName(i));
        viewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userId = routes.get(i).uzytkownikId;
                routeId = routes.get(i).trasaId;
                view.startRouteToVerifyDetailsActivity(userId, routeId);
            }
        });
    }

    private String getTouristName(int i) {
        return tourist[i];
    }

    private String getRouteReportDate(int i) {
        return dates[i];
    }

    private String getRouteName(int i) {
        return routeNames[i];
    }

    public int getRoutesToVerifyCount() {
        return routeNames.length;
    }
}
