package com.example.po.view.achivments;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.po.R;
import com.example.po.presenter.AchivmentsActivityPresenter;
import com.example.po.view.myachivments.MyAchivmentsActivity;

public class AchivmentsActivity extends AppCompatActivity implements IAchivmentsView {

    private AchivmentsActivityPresenter presenter;
    private RecyclerView achivmentsRecyclerView;
    private Button shareAchivmentsBTN;
    public AchivmentsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achivments);
        setTitle("Osiągnięcia");

        presenter = new AchivmentsActivityPresenter(this);

        achivmentsRecyclerView = findViewById(R.id.usersAchivmentsRecyclerView);
        adapter = new AchivmentsAdapter(this,presenter);
        achivmentsRecyclerView.setAdapter(adapter);
        achivmentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        shareAchivmentsBTN = findViewById(R.id.shareAchivmentBTN);
        setUpAchivmentButton();
    }

    private void setUpAchivmentButton() {
        shareAchivmentsBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openMyAchivmentsActivity();
            }
        });
    }

    @Override
    public void startMyAchivments() {
        Intent intent = new Intent(this, MyAchivmentsActivity.class);
        startActivity(intent);
    }
    @Override
    public void notifyChanged()
    {
        adapter.notifyDataSetChanged();
    }
}
