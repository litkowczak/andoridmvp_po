package com.example.po.view.achivments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.AchivmentsActivityPresenter;
import com.example.po.view.maps.MapsRecyclerViewAdapter;

public class AchivmentsAdapter extends RecyclerView.Adapter<AchivmentsAdapter.ViewHolder> {

    private Context context;
    private AchivmentsActivityPresenter presenter;

    public AchivmentsAdapter(Context context, AchivmentsActivityPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.users_achivments_row,viewGroup,false);
        return new AchivmentsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        presenter.onBindViewHolder(viewHolder,i);
    }

    @Override
    public int getItemCount() {
        return presenter.getAchivmentCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements IAchivmentsViewItem{
        private TextView achivmentNameTV;
        private TextView achivmentUserNameTV;
        private TextView achivmentDateTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            achivmentNameTV = itemView.findViewById(R.id.achivmentsAchivmentNameTV );
            achivmentUserNameTV = itemView.findViewById(R.id.achivmentsTouristNameTV);
            achivmentDateTV = itemView.findViewById(R.id.achivmentsAchivmentDate);
        }

        @Override
        public void setAchivmentName(String achivmentName){
            achivmentNameTV.setText(achivmentName);
        }

        @Override
        public void setAchivmentUserNameTV(String achivmentUserName){
            achivmentUserNameTV.setText(achivmentUserName);
        }
        @Override
        public void setAchivmentDateTV(String achivmentDate){
            achivmentDateTV.setText(achivmentDate);
        }
    }
}
