package com.example.po.view.achivments;

public interface IAchivmentsViewItem {
    void setAchivmentName(String achivmentName);

    void setAchivmentUserNameTV(String achivmentUserName);

    void setAchivmentDateTV(String achivmentDate);
}
