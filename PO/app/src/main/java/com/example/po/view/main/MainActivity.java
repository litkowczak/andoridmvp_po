package com.example.po.view.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.po.R;
import com.example.po.presenter.MainActivityPresenter;


public class MainActivity extends AppCompatActivity implements IMainView{

    private MainActivityPresenter presenter;
    private Button mapsBTN;
    private Button achivmentsBTN;
    private Button verifyBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Menu główne");

        presenter = new MainActivityPresenter(this);

        setupMapsBTN();
        setupAchivmentsBTN();
        setupVerifyBTN();

    }

    private boolean setupVerifyBTN() {
        verifyBTN = findViewById(R.id.routesVerificationBTN);
        verifyBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openRoutesVerificationActivity(MainActivity.this);
            }
        });
        return true;
    }

    private boolean setupAchivmentsBTN() {
        achivmentsBTN = findViewById(R.id.button2);
        achivmentsBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openAchivmentsActivity(MainActivity.this);
            }
        });
        return true;
    }

    private boolean setupMapsBTN() {
        mapsBTN = findViewById(R.id.mapsBTN);
        mapsBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openMapsActivity(MainActivity.this);
            }
        });
        return true;
    }


}
