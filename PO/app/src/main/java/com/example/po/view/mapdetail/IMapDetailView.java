package com.example.po.view.mapdetail;

public interface IMapDetailView {
    void startRouteCreatorActivity(String mapIndex);
    void notifyChanged();
}
