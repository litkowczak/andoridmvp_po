package com.example.po.view.mapdetail;

import android.view.View;

public interface IRouteItemView {
    void setRouteName(String routeName);
    void setRouteAmountSection(int routeAmountSection);

}
