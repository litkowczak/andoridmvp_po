package com.example.po.view.mapdetail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.MapDetailActivityPresenter;
import com.example.po.view.maps.IMapsView;
import com.example.po.view.routecreator.RouteCreatorActivity;

public class MapDetailActivity extends AppCompatActivity implements IMapDetailView {

    private RecyclerView recyclerView;
    RouteRecyclerViewAdapter adapter;
    private MapDetailActivityPresenter presenter;
    private Button startRouteCreatorActivityBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_detail2);
        Intent intent = getIntent();
        String name = intent.getStringExtra("NAME");
        String index = intent.getStringExtra("Index");
        setTitle(name);
        presenter = new MapDetailActivityPresenter(this, index);
        setupButton();


        recyclerView = findViewById(R.id.routeRecyclerView);

        adapter = new RouteRecyclerViewAdapter(this,presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager((this)));
    }

    private void setupButton() {
        startRouteCreatorActivityBTN = findViewById(R.id.createRouteBTN);
        startRouteCreatorActivityBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openCreateRouteActivity();
            }
        });
    }
    //do mapy dodac jakies id i w on clicku nie przekazywac nazwy tylko jakis id


    @Override
    public void startRouteCreatorActivity(String mapIndex) {
        Intent intent = new Intent(this, RouteCreatorActivity.class);
        intent.putExtra("Index", mapIndex);
        startActivity(intent);
    }

    @Override
    public void notifyChanged()
    {
        adapter.notifyDataSetChanged();
    }
}
