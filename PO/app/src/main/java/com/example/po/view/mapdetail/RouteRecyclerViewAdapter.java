package com.example.po.view.mapdetail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.MapDetailActivityPresenter;

class RouteRecyclerViewAdapter extends RecyclerView.Adapter<RouteRecyclerViewAdapter.MyViewHolder> {

    private Context context;
    private MapDetailActivityPresenter presenter;

    public RouteRecyclerViewAdapter(Context context, MapDetailActivityPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.routesrecyclerview_row,viewGroup,false);
        return new RouteRecyclerViewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        presenter.onBindViewHolder(myViewHolder,i);
    }

    @Override
    public int getItemCount() {
        return presenter.getRouteCount();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements IRouteItemView {
        TextView routeNameTV;
        TextView routeSectionTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            routeNameTV = itemView.findViewById(R.id.routeNameTV);
            routeSectionTV = itemView.findViewById(R.id.routeSectionAmountTV);
        }

        @Override
        public void setRouteName(String routeName){
            routeNameTV.setText(routeName);
        };

        @Override
        public void setRouteAmountSection(int routeAmountSection) {
            routeSectionTV.setText("Liczba odcinków: " + routeAmountSection);
        }


    }
}
