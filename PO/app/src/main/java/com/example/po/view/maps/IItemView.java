package com.example.po.view.maps;

import android.view.View;

public interface IItemView {

    void setMapName(String name);
    void setMapSetionAmount(int sectionAmount);

    void setOnItemClickListener(View.OnClickListener onClickListener);
}
