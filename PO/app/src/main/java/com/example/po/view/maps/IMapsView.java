package com.example.po.view.maps;

public interface IMapsView {

    void startMapDetailActivity(String name, String index);
    void notifyChanged();
}
