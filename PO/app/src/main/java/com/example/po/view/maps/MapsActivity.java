package com.example.po.view.maps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.po.R;
import com.example.po.presenter.MapsActivityPresenter;
import com.example.po.view.mapdetail.MapDetailActivity;

public class MapsActivity extends AppCompatActivity implements IMapsView {
    private RecyclerView recyclerView;

    private MapsActivityPresenter presenter;
    MapsRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setTitle("Mapy obszarów");

        presenter = new MapsActivityPresenter(this);

        recyclerView = findViewById(R.id.mapsrecyclerview);
        adapter = new MapsRecyclerViewAdapter(this,presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager((this)));
    }

    @Override
    public void startMapDetailActivity(String name, String index) {
        //Tutaj można przekazać id mapy, w nastepnym activity przekazujemy do prezentera,
        // prezenter robi query na trasy dla mapy o danym idku
        Intent intent = new Intent(this, MapDetailActivity.class);
        intent.putExtra("NAME",name);
        intent.putExtra("Index", index);
        startActivity(intent);
    }

    @Override
    public void notifyChanged()
    {
        adapter.notifyDataSetChanged();
    }
}
