package com.example.po.view.maps;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.MapsActivityPresenter;

public class MapsRecyclerViewAdapter extends RecyclerView.Adapter<MapsRecyclerViewAdapter.MyViewHolder> {

    private MapsActivityPresenter presenter;
    Context context;


    public MapsRecyclerViewAdapter(Context context, MapsActivityPresenter presenter){
        this.presenter = presenter;
        this.context = context;

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.mapsrecyclerview_row,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        presenter.onBindViewHolder(myViewHolder,i);
    }

    @Override
    public int getItemCount() {
        return presenter.getMapsCount();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder implements  IItemView {
        TextView mapNameTV;
        TextView mapSectionNumberTV;
        ImageView mapIV;

        private MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mapIV = itemView.findViewById(R.id.mapIV);
            mapSectionNumberTV = itemView.findViewById(R.id.mapSectionNumberTV);
            mapNameTV = itemView.findViewById(R.id.mapNameTV);
        }

        @Override
        public void setMapName(String name) {
            mapNameTV.setText(name);
        }

        @Override
        public void setMapSetionAmount(int sectionAmount) {
            mapSectionNumberTV.setText("Ilość odcinków :" + sectionAmount);
        }

        @Override
        public void setOnItemClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }

    }

}
