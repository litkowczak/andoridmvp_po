package com.example.po.view.myachivments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.MyAchivmentsActivityPresenter;

public class MyAchivmentsActivity extends AppCompatActivity implements IMyAchivmentsView {

    private RecyclerView recyclerView;
    private TextView actualUserName;
    private Button shareBTN;
    MyAchivmentsAdapter adapter;


    private MyAchivmentsActivityPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_achivments);
        setTitle("Osiągnięcia");
        presenter = new MyAchivmentsActivityPresenter(this);

        recyclerView = findViewById(R.id.myAchivmentsRecyclerView);
        actualUserName = findViewById(R.id.actualUserNameTV);
        shareBTN = findViewById(R.id.shareBTN);

        adapter = new MyAchivmentsAdapter(this, presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        presenter.setActualUsername();
        setUpShareBTN();

    }

    private void setUpShareBTN() {
        shareBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onShareBtnClick();
            }
        });
    }

    @Override
    public void setUsername(String name) {
        actualUserName.setText(name);
    }

    @Override
    public void notifyChanged()
    {
        adapter.notifyDataSetChanged();
    }
}
