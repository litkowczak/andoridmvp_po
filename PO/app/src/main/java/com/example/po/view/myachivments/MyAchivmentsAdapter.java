package com.example.po.view.myachivments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.MyAchivmentsActivityPresenter;

public class MyAchivmentsAdapter extends RecyclerView.Adapter<MyAchivmentsAdapter.ViewHolder> {
    private Context context;
    private MyAchivmentsActivityPresenter presenter;

    public MyAchivmentsAdapter(Context context, MyAchivmentsActivityPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_achivments_row,viewGroup,false);
        return new MyAchivmentsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        presenter.onBindViewHolder(viewHolder,i);
    }

    @Override
    public int getItemCount() {
        return presenter.getMyAchivmentsCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements MyAchivmentsItemView {
        private TextView myAchivmentNameTV;
        private TextView myAchivmentDateTV;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            myAchivmentNameTV = itemView.findViewById(R.id.myAchivmentNameTV);
            myAchivmentDateTV = itemView.findViewById(R.id.myAchivmentDateTV);
        }

        @Override
        public void setMyAchivmentName(String name){
            myAchivmentNameTV.setText(name);
        }

        @Override
        public void setMyAchivmentDate(String date){
            myAchivmentDateTV.setText(date);
        }
    }
}
