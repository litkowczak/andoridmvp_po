package com.example.po.view.rating;

public interface IRatingView {
    void setRatingBars(double diff, double state, double attractivity);
}
