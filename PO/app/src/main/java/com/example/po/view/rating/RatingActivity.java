package com.example.po.view.rating;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;

import com.example.po.R;
import com.example.po.presenter.RatingActivityPresenter;

public class RatingActivity extends AppCompatActivity implements IRatingView{
    private RatingActivityPresenter presenter;

    private RatingBar diffRatingBar;
    private RatingBar stateRatingBar;
    private RatingBar attractivityRatingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        setTitle("Ocena trasy");

        presenter = new RatingActivityPresenter(this);

        diffRatingBar = findViewById(R.id.ratingBarDifficulty);
        stateRatingBar = findViewById(R.id.ratingBarState);
        attractivityRatingBar = findViewById(R.id.ratingBarAttractivity);


        presenter.setUpRatingBars();

    }



    @Override
    public void setRatingBars(double diff, double state, double attractivity) {
    diffRatingBar.setRating((float)diff);
    stateRatingBar.setRating((float)state);
    attractivityRatingBar.setRating((float)attractivity);


    }
}
