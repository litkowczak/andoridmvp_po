package com.example.po.view.routecreator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.RouteCreatorActivityPresenter;

public class RouteCreatorActivity extends AppCompatActivity implements IRouteCreator {

    private RecyclerView sectionRecyclerView;
    private RouteCreatorActivityPresenter presenter;
    SectionRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_creator);
        setTitle("Kreator trasy");

        sectionRecyclerView = findViewById(R.id.sectionRecyclerView);
        Intent intent = getIntent();
        String index = intent.getStringExtra("Index");
        presenter = new RouteCreatorActivityPresenter(this, index);

        adapter = new SectionRecyclerViewAdapter(this, presenter);
        sectionRecyclerView.setAdapter(adapter);
        sectionRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        Button button = (Button) findViewById(R.id.saveRouteBTN);
        final EditText textView = (EditText)  findViewById(R.id.routeNameEditText);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                presenter.saveRoute(textView.getText().toString());
            }
        });
    }

    @Override
    public void notifyChanged()
    {
        adapter.notifyDataSetChanged();
    }
}
