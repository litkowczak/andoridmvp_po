package com.example.po.view.routecreator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.RouteCreatorActivityPresenter;


public class SectionRecyclerViewAdapter extends RecyclerView.Adapter<SectionRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private RouteCreatorActivityPresenter presenter;

    public SectionRecyclerViewAdapter(Context context, RouteCreatorActivityPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.sectionrecyclerview_row,viewGroup,false);
        return new SectionRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        presenter.onBindViewHolder(viewHolder, i);
    }

    @Override
    public int getItemCount() {
        return presenter.getSectionCount();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder implements ISectionItemView{
        private TextView sectionNameTV;
        private TextView sectionLenghtTV;
        public CheckBox checkBox;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sectionNameTV = itemView.findViewById(R.id.sectionNameTV);
            sectionLenghtTV = itemView.findViewById(R.id.sectionLengthTV);
            checkBox= itemView.findViewById(R.id.sectionChB);
        }

        @Override
        public void setSectionName(String name) {
            sectionNameTV.setText(name);
        }

        @Override
        public void setSetionLenght(int length) {
            sectionLenghtTV.setText("Długość odcinka : " + length + "km");
        }


    }
}
