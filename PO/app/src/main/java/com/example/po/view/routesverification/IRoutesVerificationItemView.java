package com.example.po.view.routesverification;

import android.view.View;

public interface IRoutesVerificationItemView {
    void setRouteNameTV(String name);

    void setTouristNameTV(String tName);

    void setRouteVerifyReportDate(String date);

    void setOnClickListener(View.OnClickListener onClickListener);
}
