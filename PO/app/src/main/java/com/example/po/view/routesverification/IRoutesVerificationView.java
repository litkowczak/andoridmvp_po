package com.example.po.view.routesverification;

public interface IRoutesVerificationView {
    void startRouteToVerifyDetailsActivity(Integer userId, Integer routeId);
    void notifyChanged();
}
