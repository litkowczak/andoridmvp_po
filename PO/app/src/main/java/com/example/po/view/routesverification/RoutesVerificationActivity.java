package com.example.po.view.routesverification;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.po.R;
import com.example.po.presenter.RoutesVerificationActivityPresenter;
import com.example.po.view.routetoverifydetails.RouteToVerifyDetailsActivity;

public class RoutesVerificationActivity extends AppCompatActivity implements IRoutesVerificationView {

    private RoutesVerificationActivityPresenter presenter;
    private RecyclerView recyclerView;
    private RoutesVerificationRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes_verification);
        setTitle("Trasy do weryfikacji");

        presenter = new RoutesVerificationActivityPresenter(this);


        recyclerView = findViewById(R.id.RoutesToVeriftyRV);
        adapter = new RoutesVerificationRecyclerViewAdapter(this, presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void startRouteToVerifyDetailsActivity(Integer userId, Integer routeId) {
        Intent intent = new Intent(this, RouteToVerifyDetailsActivity.class);
        intent.putExtra("userID", Integer.toString(userId));
        intent.putExtra("routeID", Integer.toString(routeId));
        startActivity(intent);
    }

    @Override
    public void notifyChanged()
    {
        adapter.notifyDataSetChanged();
    }
}
