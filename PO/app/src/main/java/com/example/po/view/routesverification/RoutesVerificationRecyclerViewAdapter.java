package com.example.po.view.routesverification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.RoutesVerificationActivityPresenter;

public class RoutesVerificationRecyclerViewAdapter extends RecyclerView.Adapter<RoutesVerificationRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private RoutesVerificationActivityPresenter presenter;

    public RoutesVerificationRecyclerViewAdapter(Context context, RoutesVerificationActivityPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.routestoverify_recyclerview_row,viewGroup,false);
        return new RoutesVerificationRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoutesVerificationRecyclerViewAdapter.ViewHolder viewHolder, int i) {
        presenter.onBindViewHolder(viewHolder,i);
    }

    @Override
    public int getItemCount() {
        return presenter.getRoutesToVerifyCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements IRoutesVerificationItemView {
        private TextView touristNameTV;
        private TextView routeNameTV;
        private TextView routeVerifyReportDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            touristNameTV = itemView.findViewById(R.id.touristVerifyName);
            routeNameTV = itemView.findViewById(R.id.routeVerifyName);
            routeVerifyReportDate = itemView.findViewById(R.id.routeVerifyReportDate);


        }

        @Override
        public void setRouteNameTV(String name){
            routeNameTV.setText(name);
        }

        @Override
        public void setTouristNameTV(String tName){
            touristNameTV.setText(tName);
        }
        @Override
        public void setRouteVerifyReportDate(String date){
            routeVerifyReportDate.setText(date);
        }

        @Override
        public void setOnClickListener(View.OnClickListener onClickListener){
            itemView.setOnClickListener(onClickListener);
        }

    }


}
