package com.example.po.view.routetoverifydetails;

public interface IRoutesToVerifyDetails {
    void setReportDate(String date);

    void setTouristName(String nameOfTourist);

    void startRatingActivity();

    void notifyChanged();
}
