package com.example.po.view.routetoverifydetails;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.po.R;
import com.example.po.presenter.RouteToVerifyDetailsActivityPresenter;

public class RouteToVSectionAdapter extends RecyclerView.Adapter<RouteToVSectionAdapter.ViewHolder> {
    private Context context;
    private RouteToVerifyDetailsActivityPresenter presenter;

    public RouteToVSectionAdapter(Context context, RouteToVerifyDetailsActivityPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.route_to_v_section_row,viewGroup,false);
        return new RouteToVSectionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        presenter.onBindViewHolder(viewHolder,i);
    }

    @Override
    public int getItemCount() {
        return presenter.getSectionCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements IRouteToVerifyDetailsItemView{
        private TextView sectionNameTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sectionNameTV = itemView.findViewById(R.id.routeToVSectionName);
        }

        @Override
        public void setSectionNameTV(String sectionName) {
            sectionNameTV.setText(sectionName);
        }
    }
}
