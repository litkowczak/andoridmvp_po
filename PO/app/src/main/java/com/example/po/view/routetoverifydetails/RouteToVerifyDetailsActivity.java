package com.example.po.view.routetoverifydetails;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.po.R;
import com.example.po.presenter.RouteToVerifyDetailsActivityPresenter;
import com.example.po.view.rating.RatingActivity;


public class RouteToVerifyDetailsActivity extends AppCompatActivity implements IRoutesToVerifyDetails {

    private RouteToVerifyDetailsActivityPresenter presenter;
    private TextView routetvDetailTouristName;
    private TextView routetvDetailReportDate;
    private RecyclerView recyclerViewRouteToV;

    private Button rateBTN;

    private Button acceptBTN;
    private Button rejectBTN;
    RouteToVSectionAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_to_verify_details);
        setTitle("Szczegóły trasy");

        rateBTN = findViewById(R.id.routeFeedbackBTN);
        acceptBTN = findViewById(R.id.acceptBTN);
        rejectBTN = findViewById(R.id.rejectBTN);

        Intent intent = this.getIntent();
        int userId = Integer.parseInt(intent.getStringExtra("userID"));
        int routeId = Integer.parseInt(intent.getStringExtra("routeID"));
        presenter = new RouteToVerifyDetailsActivityPresenter(this, userId, routeId);
        routetvDetailTouristName = findViewById(R.id.touristRouteToVerifyDetailsTV);
        routetvDetailReportDate = findViewById(R.id.routetvDetailReportDate);
        presenter.setUpTextViews();

        recyclerViewRouteToV = findViewById(R.id.routeToVRecyclerView);


        adapter = new RouteToVSectionAdapter(this,presenter);

        recyclerViewRouteToV.setAdapter(adapter);
        recyclerViewRouteToV.setLayoutManager(new LinearLayoutManager(this));

        rateBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startRatingActivity();
            }
        });
        acceptBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.acceptRoute();
            }
        });
        rejectBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.rejectRoute();
            }
        });
    }

    @Override
    public void setReportDate(String date) {
        routetvDetailReportDate.setText(date);
    }

    @Override
    public void setTouristName(String nameOfTourist) {
        routetvDetailTouristName.setText(nameOfTourist);
    }

    @Override
    public void startRatingActivity() {
        Intent intent = new Intent(this, RatingActivity.class);
        startActivity(intent);
    }
    @Override
    public void notifyChanged()
    {
        adapter.notifyDataSetChanged();
    }

}
