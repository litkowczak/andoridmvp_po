package com.example.po;

import android.content.SharedPreferences;

import com.example.po.presenter.AchivmentsActivityPresenter;
import com.example.po.presenter.MyAchivmentsActivityPresenter;
import com.example.po.view.achivments.AchivmentsActivity;
import com.example.po.view.achivments.IAchivmentsView;
import com.example.po.view.myachivments.MyAchivmentsActivity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MyAchievementsTest {

    @Mock
    private MyAchivmentsActivity view;

    private MyAchivmentsActivityPresenter presenter;
    MockWebServer mockWebServer;

    @Before
    public void setUpEditorPresenter() throws Exception{
        mockWebServer = new MockWebServer();
        mockWebServer.start();

        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody("[{}]");
        mockWebServer.enqueue(mockedResponse);
        MockitoAnnotations.initMocks(this);
        this.presenter = new MyAchivmentsActivityPresenter(view);
    }

    @Test
    public void createEditorPresenter_newNote() throws Exception {
        Thread.sleep(5000);
        verify(view,times(1)).notifyChanged();
    }
}
